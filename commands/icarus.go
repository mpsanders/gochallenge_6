// Copyright © 2015 Steve Francia <spf@spf13.com>.
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package commands

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"mark.sanders/go_challenges/labrynth_6/mazelib"
)

// Defining the icarus command.
// This will be called as 'laybrinth icarus'
var icarusCmd = &cobra.Command{
	Use:     "icarus",
	Aliases: []string{"client"},
	Short:   "Start the laybrinth solver",
	Long: `Icarus wakes up to find himself in the middle of a Labyrinth.
  Due to the darkness of the Labyrinth he can only see his immediate cell and if
  there is a wall or not to the top, right, bottom and left. He takes one step
  and then can discover if his new cell has walls on each of the four sides.

  Icarus can connect to a Daedalus and solve many laybrinths at a time.`,
	Run: func(cmd *cobra.Command, args []string) {
		RunIcarus()
	},
}

func init() {
	RootCmd.AddCommand(icarusCmd)
	rand.Seed(time.Now().UTC().UnixNano())
}

func RunIcarus() {
	// Run the solver as many times as the user desires.
	fmt.Println("Solving", viper.GetInt("times"), "times")
	for x := 0; x < viper.GetInt("times"); x++ {
		solveMaze()
	}

	// Once we have solved the maze the required times, tell daedalus we are done
	makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/done")
}

// Make a call to the laybrinth server (daedalus) that icarus is ready to wake up
func awake() mazelib.Survey {
	contents, err := makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/awake")
	if err != nil {
		fmt.Println(err)
	}
	r := ToReply(contents)
	return r.Survey
}

// Make a call to the laybrinth server (daedalus)
// to move Icarus a given direction
// Will be used heavily by solveMaze
func Move(direction string) (mazelib.Survey, error) {
	if direction == "left" || direction == "right" || direction == "up" || direction == "down" {
		contents, err := makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/move/" + direction)
		if err != nil {
			return mazelib.Survey{}, err
		}

		rep := ToReply(contents)
		if rep.Victory == true {
			fmt.Println(rep.Message)
			return rep.Survey, mazelib.ErrVictory
		}

		return rep.Survey, nil
	}

	return mazelib.Survey{}, errors.New("invalid direction")
}

// utility function to wrap making requests to the daedalus server
func makeRequest(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return contents, nil
}

// Handling a JSON response and unmarshalling it into a reply struct
func ToReply(in []byte) mazelib.Reply {
	res := &mazelib.Reply{}
	json.Unmarshal(in, &res)
	return *res
}

// Where the magic happens
func solveMaze() {
	room := awake()
	if err := solveBackTrackWalker(room); err != nil {
		fmt.Println(err.Error())
	}
}

func solveBackTrackWalker(room mazelib.Survey) error {
	roomInfoStack := Stack{make([]*RoomInfo, 0)}

	var direction string
	var err error
	for i := 0; i < viper.GetInt("max-steps") || viper.GetInt("max-steps") == -1; i++ {
		// Maintain a history of the room - including the path that led in here if applicable
		currentRoomInfo := NewRoomInfo(room, reverseDirection(direction))

		// If dead end - backtrack until we find a room with an untaken path
		for currentRoomInfo.TriedAllPaths() {
			prevRoomInfo, err := roomInfoStack.Pop()
			if err != nil {
				return errors.New("This is a tomb - treasure unreachable!")
			}
			lastTakenPath, err := prevRoomInfo.LastTakenPath()
			panicIfError(err)

			if room, err = Move(reverseDirection(lastTakenPath)); err != nil {
				return err
			}
			i += 1 // Must account for the step
			currentRoomInfo = prevRoomInfo
		}

		// Take one of the untaken paths at random
		direction, err = currentRoomInfo.RandomUntakenPath()
		panicIfError(err)

		currentRoomInfo.AddTakenPath(direction)
		roomInfoStack.Push(currentRoomInfo)
		if room, err = Move(direction); err != nil {
			return err
		}
	}
	return nil
}

func solveRandomWalker(room mazelib.Survey) error {
	var err error
	for i := 0; err == nil && i < viper.GetInt("max-steps"); i++ {
		paths := getPaths(room)
		chosenPath := rand.Int() % len(paths)
		room, err = Move(paths[chosenPath])
	}
	return err
}

func getPaths(room mazelib.Survey) []string {
	possibleDirections := make([]string, 0, 4)
	if !room.Left {
		possibleDirections = append(possibleDirections, "left")
	}
	if !room.Top {
		possibleDirections = append(possibleDirections, "up")
	}
	if !room.Right {
		possibleDirections = append(possibleDirections, "right")
	}
	if !room.Bottom {
		possibleDirections = append(possibleDirections, "down")
	}
	return possibleDirections
}

func reverseDirection(dir string) string {
	if dir == "left" {
		return "right"
	} else if dir == "up" {
		return "down"
	} else if dir == "right" {
		return "left"
	} else if dir == "down" {
		return "up"
	}
	return ""
}

func existsInCollection(items []string, item string) bool {
	for _, existingItem := range items {
		if existingItem == item {
			return true
		}
	}
	return false
}

func panicIfError(err error) {
	if err != nil {
		panic(err.Error())
	}
}

// Room to maintain history
type RoomInfo struct {
	paths      []string
	takenPaths []string
}

func (r RoomInfo) String() string {
	return fmt.Sprintf("Paths:%d:%v, TakenPaths:%d:%v",
		len(r.paths), r.paths, len(r.takenPaths), r.takenPaths)
}

// Create an info object with the possible paths and the path led here if applicable
func NewRoomInfo(room mazelib.Survey, entryDirection string) *RoomInfo {
	takenPaths := make([]string, 0)
	if entryDirection != "" { // start room has no entry direction
		takenPaths = append(takenPaths, entryDirection)
	}

	return &RoomInfo{
		paths:      getPaths(room),
		takenPaths: takenPaths,
	}
}

func (r *RoomInfo) TriedAllPaths() bool {
	return len(r.takenPaths) == len(r.paths)
}

func (r *RoomInfo) AddTakenPath(direction string) {
	r.takenPaths = append(r.takenPaths, direction)
}

func (r *RoomInfo) LastTakenPath() (string, error) {
	if len(r.takenPaths) == 0 {
		return "", errors.New("No paths taken")
	}
	return r.takenPaths[len(r.takenPaths)-1], nil
}

func (r *RoomInfo) RandomUntakenPath() (string, error) {
	untakenPaths := make([]string, 0)
	for _, path := range r.paths {
		if !existsInCollection(r.takenPaths, path) {
			untakenPaths = append(untakenPaths, path)
		}
	}

	if len(untakenPaths) == 0 {
		return "", errors.New("No paths taken")
	}
	return untakenPaths[rand.Int()%len(untakenPaths)], nil
}

// Stack to maintain history
type Stack struct {
	rooms []*RoomInfo
}

func (s *Stack) Push(room *RoomInfo) {
	s.rooms = append(s.rooms, room)
}

func (s *Stack) Pop() (*RoomInfo, error) {
	if len(s.rooms) == 0 {
		return &RoomInfo{}, errors.New("No more history")
	}

	direction := s.rooms[len(s.rooms)-1]
	s.rooms = s.rooms[:len(s.rooms)-1]
	return direction, nil
}
