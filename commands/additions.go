package commands

import (
	"errors"
	"math"
	"math/rand"

	"github.com/spf13/cobra"

	"mark.sanders/go_challenges/labrynth_6/mazelib"
)

//*** Additions to the Maze struct

// Set start & end points - Ensure they're not the same
func (m *Maze) setRandomStartAndEnd() {
	c := m.randomLocation()

	err := m.SetStartPoint(c.X, c.Y)
	for err != nil {
		c = m.randomLocation()
		err = m.SetStartPoint(c.X, c.Y)
	}

	err = m.SetTreasure(c.X, c.Y)
	for err != nil {
		c = m.randomLocation()
		err = m.SetTreasure(c.X, c.Y)
	}
}

func (m *Maze) bashThroughWall(c1, c2 mazelib.Coordinate) error {
	r1, _ := m.GetRoom(c1.X, c1.Y)
	r2, _ := m.GetRoom(c2.X, c2.Y)

	if math.Abs(float64(c1.X-c2.X)) > 1 || math.Abs(float64(c1.Y-c2.Y)) > 1 {
		return errors.New("Cells are not adjacent")
	} else if c1.X < c2.X { // c1 is left
		r1.RmWall(mazelib.E)
		r2.RmWall(mazelib.W)
	} else if c1.Y < c2.Y { // c1 is up
		r1.RmWall(mazelib.S)
		r2.RmWall(mazelib.N)
	} else if c1.X > c2.X { // c1 is right
		r1.RmWall(mazelib.W)
		r2.RmWall(mazelib.E)
	} else if c1.Y > c2.Y { // c1 is down
		r1.RmWall(mazelib.N)
		r2.RmWall(mazelib.S)
	}
	return nil
}

// Bash some walls down for difficulty
func (m *Maze) bashSomeRandomWalls(num int) {
	for i := 0; i < 40; i++ {
		r := m.randomLocation()
		if r.X > 0 && r.X < m.Width()-1 && r.Y > 0 && r.Y < m.Height()-1 {
			dirs := [...]mazelib.Coordinate{
				mazelib.Coordinate{X: r.X - 1, Y: r.Y}, // left
				mazelib.Coordinate{X: r.X, Y: r.Y - 1}, // up
				mazelib.Coordinate{X: r.X + 1, Y: r.Y}, // right
				mazelib.Coordinate{X: r.X, Y: r.Y + 1}, // down
			}
			m.bashThroughWall(r, dirs[len(dirs)-1])
		}
	}
}

//Gets a random location somewhere within the maze
func (m *Maze) randomLocation() mazelib.Coordinate {
	return mazelib.Coordinate{
		X: rand.Intn(m.Width()),
		Y: rand.Intn(m.Height()),
	}
}

// Gets the set of visited neighbouring cells within the maze
func (m *Maze) getVisitedNeighbours(c mazelib.Coordinate) []mazelib.Coordinate {
	return m.getNeighbours(c, true)
}

// Gets the set of unvisited neighbouring cells within the maze
func (m *Maze) getUnvisitedNeighbours(c mazelib.Coordinate) []mazelib.Coordinate {
	return m.getNeighbours(c, false)
}

// Gets the set of visited neighbouring cells within the maze
func (m *Maze) getNeighbours(c mazelib.Coordinate, getVisitedCells bool) []mazelib.Coordinate {
	neighbours := make([]mazelib.Coordinate, 0, 4)
	for _, n := range m.getAllNeighbours(c) {
		if room, err := m.GetRoom(n.X, n.Y); err == nil {
			if getVisitedCells && room.Visited {
				neighbours = append(neighbours, n)
			} else if !getVisitedCells && !room.Visited {
				neighbours = append(neighbours, n)
			}
		}
	}
	return neighbours
}

// Gets the set of all neighbouring cells within the maze
func (m *Maze) getAllNeighbours(c mazelib.Coordinate) []mazelib.Coordinate {
	neighbours := make([]mazelib.Coordinate, 0, 4)
	neighbours = append(neighbours, mazelib.Coordinate{c.X - 1, c.Y})
	neighbours = append(neighbours, mazelib.Coordinate{c.X, c.Y - 1})
	neighbours = append(neighbours, mazelib.Coordinate{c.X + 1, c.Y})
	neighbours = append(neighbours, mazelib.Coordinate{c.X, c.Y + 1})
	return neighbours
}

// Print command icarus print
var printCmd = &cobra.Command{
	Use:   "print",
	Short: "Print a generated maze",
	Long:  `Useful for testing. Prints a generated maze to console`,
	Run: func(cmd *cobra.Command, args []string) {
		mazelib.PrintMaze(createMaze())
	},
}

func init() {
	RootCmd.AddCommand(printCmd)
}
